package org.danilenko.cluster.boot

import akka.actor.{ActorSystem, Props}
import org.danilenko.cluster.actor.MainActor
import org.danilenko.cluster.helpers.ConsoleHelper

object Main extends App {
  implicit val actorSystem = ActorSystem("MainActorSystem")
  val mainActor = actorSystem.actorOf(Props[MainActor], "MainActor")

  ConsoleHelper.readInput(mainActor)
  actorSystem.terminate()
}
