package org.danilenko.cluster.actor

import java.util.concurrent.TimeUnit

import akka.actor._
import akka.remote._
import akka.routing._
import common._
import org.danilenko.remote.actor.RemoteActor

import scala.collection.immutable.IndexedSeq
import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration
import scala.sys.process.Process


case class ActorSettings(var lastNodePort: Int, var messagesInterval: Int, var nodesCount: Int, var router: Router)

class MainActor extends Actor with ActorLogging {

  val StartMessagesInterval = 100
  val StartNodesInCluster = 0

  var router = {
    val routs = Vector.empty[ActorRefRoutee]
    Router(RoundRobinRoutingLogic(), routs)
  }

  val actorSettings = ActorSettings(1000, StartMessagesInterval, StartNodesInCluster, router)
  val processes = mutable.Map.empty[ActorRef, Process]

  implicit val executionContext = context.dispatcher

  override def preStart(): Unit = {
    self ! Start
  }

  override def postStop() = {
    log.info("Inside postStop method of MainActor")
    processes.values.foreach(_.destroy())
    actorSettings.router.route(Broadcast(PoisonPill), self)
  }

  override def receive: Receive = {
    case Start =>
      log.info("Main actor listen")

    case Terminated(terminatedNode) =>
      context.unwatch(terminatedNode)
      processes.get(terminatedNode).foreach { process =>
        process.destroy()
        processes.remove(terminatedNode)
      }
      actorSettings.router = actorSettings.router.removeRoutee(terminatedNode)
      actorSettings.router.route(Broadcast(DeleteNode(ActorRefRoutee(terminatedNode))), terminatedNode)

    case RunNewNode =>
      addNewNode()

    case DeleteNode =>
      val routs = actorSettings.router.routees
      if (routs.nonEmpty) {
        val head = routs.head
        actorSettings.router.route(Broadcast(DeleteNode(head)), self)
        router = actorSettings.router.removeRoutee(head)
        println(s"Delete node $head")
      } else {
        println("Cluster is empty")
      }

    case Statistic(address, messages) =>
      println(s"Actor [$address] processed $messages messages")

    case GetStatistics =>
      val routs: IndexedSeq[Routee] = actorSettings.router.routees
      println(s"${routs.size} nodes in cluster\n ${actorSettings.messagesInterval} ms interval ")
      if (routs.nonEmpty) {
        actorSettings.router.route(Broadcast(GetStatistics), self)
      } else {
        println("Cluster is empty")
      }


    case newInterval: ChangeInterval =>
      if (newInterval.messagesInterval >= 1) {
        actorSettings.messagesInterval = newInterval.messagesInterval
        actorSettings.router.route(Broadcast(newInterval), self)
      } else {
        println("Wrong interval")
      }
  }

  /**
    * Create and launch new remote node
    */
  def addNewNode(): Unit = {
    actorSettings.lastNodePort += 1

    val nodeName = s"Node.RemoteActorSystem.port_${actorSettings.lastNodePort.toString}."
    val remoteNodeProcess = Process( s"""java -DnodeName=$nodeName -Dakka.remote.netty.tcp.port=${actorSettings.lastNodePort} -jar node.jar""").run

    context.system.scheduler.scheduleOnce(FiniteDuration(100, TimeUnit.MILLISECONDS)) {
      val address = Address("akka.tcp", "RemoteActorSystem", "127.0.0.1", actorSettings.lastNodePort)
      val remoteNodeActor = context.system.actorOf(Props[RemoteActor].withDeploy(Deploy(scope = RemoteScope(address))))

      initNewRemoteNode(remoteNodeActor)
      context.watch(remoteNodeActor)
      processes += remoteNodeActor -> remoteNodeProcess

      actorSettings.router = actorSettings.router.addRoutee(remoteNodeActor)
      notifyClusterWithNewNode(remoteNodeActor)
      println(s"[${address.toString}] added to cluster. Now ${actorSettings.router.routees.size} nodes in cluster.")
      log.info(s"New node[${remoteNodeActor.path.toString}] added to cluster with path. Now ${actorSettings.router.routees.size} nodes in cluster")
    }

  }

  /**
    * Initialization of cluster node
    * @param remoteNodeActor - actor from node
    */
  def initNewRemoteNode(remoteNodeActor: ActorRef): Unit = {
    remoteNodeActor ! AuthorizeMainNode
    remoteNodeActor ! ChangeInterval(actorSettings.messagesInterval)
    remoteNodeActor ! AddOtherNodes(actorSettings.router.routees)
  }

  /**
    * Notify all cluster nodes
    * @param newNodeActor - new node added to cluster
    */
  def notifyClusterWithNewNode(newNodeActor: ActorRef): Unit = {
    log.info("Main node notify other nodes that new added to cluster")
    actorSettings.router.route(Broadcast(AddNode(newNodeActor)), newNodeActor)
  }
}
