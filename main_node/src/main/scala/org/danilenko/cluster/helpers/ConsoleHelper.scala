package org.danilenko.cluster.helpers

import akka.actor.{ActorRef, PoisonPill}
import common._

object ConsoleHelper {
  @inline def defined(line: String) = {
    line != "exit"
  }

  def readInput(actor: ActorRef): Unit = {
    Iterator.continually {
      println("================================")
      println("Supported commands:")
      println("1) add - add node")
      println("2) stat - show statistic")
      println("3) del - delete node")
      println("4) interval=_  - change interval in ms")
      println("5) exit - to close application")
      println("================================")
      Console.readLine
    }.takeWhile(defined).foreach(commandDispatch(_, actor))
  }

  def commandDispatch(command: String, actorReceiver: ActorRef): Unit = {
    command.toLowerCase match {
      case "stat" => actorReceiver ! GetStatistics
      case "add" => actorReceiver ! RunNewNode
      case "del" => actorReceiver ! DeleteNode
      case str: String if str.startsWith("interval=") =>
        str.split("=").lastOption.foreach {
          number => try {
            val interval: Int = number.toInt
            if (1 <= interval && interval <= 100) {
              actorReceiver ! ChangeInterval(interval)
            } else {
              println("Wrong interval (only 1-100 ms)")
            }
          } catch {
            case f: NumberFormatException => println(s"WRONG COMMAND FORMAT (use only numbers)")
          }
        }
      case str: String if str.startsWith("exit") => actorReceiver ! PoisonPill
      case _ => println("wrong command")
    }
  }
}
