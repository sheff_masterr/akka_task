package org.danilenko.remote.actor

import java.util.concurrent.TimeUnit

import akka.actor._
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import common._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

case class RemoteActorSettings(var timeInterval: Int, var otherNodesRouter: Router, var mainNode: Option[ActorRef] = Option.empty[ActorRef])

case class ActorInfo(var messagesPerSecond: Int, var receivedMessages: Int)

class RemoteActor extends Actor with ActorLogging {
  implicit val executionContext = context.system.dispatcher

  val InitialTimeInterval = 1000
  val ScanReceivedMessages = 1000

  val actorSettings = new RemoteActorSettings(InitialTimeInterval, Router(RoundRobinRoutingLogic(), Vector.empty[ActorRefRoutee]))
  val actorInfo = new ActorInfo(0, 0)

  var messageSenderScheduler = createScheduler(actorSettings.timeInterval, sendMessageToOtherNode())
  val messageProcessedScheduler = createScheduler(ScanReceivedMessages, calcMessagesPerSecond())

  def createScheduler(interval: Int, f: => Unit)(implicit executor: ExecutionContext): Cancellable = {
    context.system.scheduler.schedule(FiniteDuration(0, TimeUnit.MILLISECONDS), FiniteDuration(interval, TimeUnit.MILLISECONDS))(f)
  }

  /**
    * Send message to one of the remote nodes.
    * On each call send message to different node (uses RoundRobinRoutingLogic)
    */
  def sendMessageToOtherNode(): Unit = {
    if (actorSettings.otherNodesRouter.routees.nonEmpty) {
      actorSettings.otherNodesRouter.route(Message(s"Test text by scheduler from $self"), self)
    }
  }

  /**
    * Calculating messages processed by actor every second
    */
  def calcMessagesPerSecond(): Unit = {
    actorInfo.messagesPerSecond = actorInfo.receivedMessages
    log.info(s"Messages processed by last second = ${actorInfo.messagesPerSecond}")
    actorInfo.receivedMessages = 0
  }

  /**
    * Is called when an Actor is started.
    */
  override def preStart(): Unit = {
    self ! Start
  }

  /**
    * Is called asynchronously after 'actor.stop()' is invoked.
    */
  override def postStop() = {
    context.system.terminate()
  }

  override def receive: Receive = {

    case AuthorizeMainNode =>
      log.info("Auth main node")
      actorSettings.mainNode = Some(sender())

    case GetStatistics =>
      sender() ! Statistic(s"${self.toString}", actorInfo.messagesPerSecond)

    case ChangeInterval(interval) =>
      log.info(s"change interval from ${actorSettings.timeInterval} to $interval")
      messageSenderScheduler.cancel()
      actorSettings.timeInterval = interval
      messageSenderScheduler = createScheduler(interval, sendMessageToOtherNode())

    case AddNode(node) =>
      if (node != self) {
        actorSettings.otherNodesRouter = actorSettings.otherNodesRouter.addRoutee(node)
        log.info(s"Add new node ${node.toString()}. Now ${actorSettings.otherNodesRouter.routees.size} other nodes available")
      }
    case AddOtherNodes(routs) =>
      actorSettings.otherNodesRouter = actorSettings.otherNodesRouter.copy(routees = routs)

    case DeleteNode(nodeRoutee) =>
      if (nodeRoutee == ActorRefRoutee(self)) {
        self ! PoisonPill
        log.info("Get PoisonPill")
      } else {
        actorSettings.otherNodesRouter = actorSettings.otherNodesRouter.removeRoutee(nodeRoutee)
        log.info(s"delete node. Now ${actorSettings.otherNodesRouter.routees.size} other nodes available")
      }

    case Message(message) =>
      log.info(message)
      actorInfo.receivedMessages += 1
  }
}
