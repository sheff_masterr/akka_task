package org.danilenko.remote.boot

import akka.actor.{Props, ActorSystem}
import org.danilenko.remote.actor.RemoteActor

object Main extends App {
  val actorSystem = ActorSystem("RemoteActorSystem")
  val mainActor = actorSystem.actorOf(Props[RemoteActor], "MainActor")

  actorSystem.registerOnTermination {
    println(s"remote node ${mainActor.path.toString} terminated")
  }
}
