package common

import akka.actor.ActorRef
import akka.routing.Routee

import scala.collection.immutable.IndexedSeq

case class Statistic(address: String, messagesPerSecond: Int)

case class ChangeInterval(messagesInterval: Int)

case object NewNodeAuthorizes

case class AddOtherNodes(nodes: IndexedSeq[Routee])

case class Message(message: String)

case object AuthorizeMainNode

case class AddNode(node: ActorRef)

case class DeleteNode(node: Routee)

case object RunNewNode

case object GetStatistics

case object ShutDown

case object Start


